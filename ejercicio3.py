import os, random
from matplotlib import pyplot as plt

def dfs(inicio,meta):
    xp = inicio[0]; yp = inicio[1]
    xm = meta[0]; ym = meta[1]
    posiciones = [[xp,yp]]
    posicionx = 0; posiciony = 0

    for i in range(1,200):
        if xp != xm and posicionx != xp:
            posiciony = xp
            if xp > 20:
                xp = 1
            else:
                xp += 1
        
        if yp != ym and posiciony != yp:
            posiciony = yp
            if yp > 20:
                yp = 1
            else:
                yp += 1
        
        posiciones.append([xp,yp])

        if xp == xm and yp == ym:
            return posiciones
            break


INICIO = [random.randint(1,20),random.randint(1,20)]
META = [random.randint(1,20),random.randint(1,20)]
os.system("cls")

print("PUNTO DE INICIO: ({},{})".format(INICIO[0],INICIO[1]))
print("META: ({},{})".format(META[0],META[1]))

datos = dfs(INICIO,META)
print("TODOS LOS PUNTOS:\n{}".format(datos))
for i in datos:
    plt.plot(i[0],i[1],'ro')

plt.plot(INICIO[0],INICIO[1],'ko')
plt.plot(META[0],META[1],'yo')
plt.xlabel("PUNTO DE INICIO:({},{})  META: ({},{})".format(INICIO[0],INICIO[1],META[0],META[1]))
plt.ylabel("Y")
plt.show()


input("\nENTER para Continuar")